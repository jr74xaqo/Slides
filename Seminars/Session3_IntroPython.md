
Here is a table of content for this lesson

1. [Python: basic concepts](#Python:-basic-concepts)
2. [Conditional statements and iterations](#Conditional-statements-and-iterations)
3. [Input and Output](#Input-and-Output)
4. [Functions](#Functions)
5. [Errors and Exceptions](#Error-and-exceptions)
6. [Libraries and Modules](#Libraries-and-modules)
7. [Your Homeworks!](#Homeworks!)

# Python: basic concepts

Python is a very flexible and very powerful **programming language** that can help you with your work with data and DH materials. Python's phylosophy emphasizes code readability and features a simple and very expressive syntax. It is actually easy to master the basic aspects of Python's syntax: it is amazing how much you can do even with just the most basic concepts... The aim of the following lectures is to introduce to you some of these basic operation, let you see some code in action and also give you some exercise where you can apply what you've seen.

It is also amazing how many thing you can accomplish with some well written lines of Python! But of course, you can even just use Python do somethin as easy as...


```python
2 + 3
```




    5



In this example, `2`, `3` and `5` are the basic "ingredients" we work with. They are called **values**. And they belong to a special class, which means they are of a particular **type**. Can you guess which?

That's right! They are numbers. Positive and negative whole numbers are called **integers**. In the example above `2`, `3` and `5` are indeed intergers. How do I now? Python as a **function** called `type` to tell what a particular value is:


```python
type(2)
```




    int



What other types does Python have?

## Variables and data types

So, we've written our first line of code... But I guess we want to do something a little more interesting, right? Well, for a start, we might want to use Python to execute some operation (say: sum two numbers like 2 and 3) and process the result to print it on the screen, process it, and reuse it as many time as we want...

**Variables** is what we use to store values. Think of it as a shoebox where you place your content; next time you need that content (i.e. the result of a previous operation, or for example some input you've read from a file) you simply call the shoebox name...


```python
result = 2 + 3
```


```python
#now we print the result
print(result)
```

    5



```python
# by the way, I'm a comment. I'm not executed
# every line of code following the sign # is ignored:
# print("I'm line n. 3: do you see me?")
# see? You don't see me...
print("I'm line nr. 5 and you DO see me!")
```

    I'm line nr. 5 and you DO see me!


That's it! As easy as that (yes, in some programming languages you have to create or declare the variable first and then use it to fill the shoebox; in Python, you go ahead and simply use it!)

Now, what do you think we will get when we execute the following code?


```python
result + 5
```




    10



So can we give variables any name we want? Obviously not! This is **fine**:

* lowercase letters (e.g. `result` or `variable`, or `blablablablabla`)
* lowercase letters with underscore (e.g. `more_than_one_word`)
* uppercase letters *could* be fine, but by convention they're used only in special cases

Does case matter? Let's see:


```python
variable = "Hello, I am a variable!"
print(Variable)
```


    ---------------------------------------------------------------------------

    NameError                                 Traceback (most recent call last)

    <ipython-input-13-947cd3937e79> in <module>()
          1 variable = "Hello, I am a variable!"
    ----> 2 print(Variable)
    

    NameError: name 'Variable' is not defined


This variable names are **NOT** ok. Can you guess why? And can you guess what happens if you try to use them?


```python
76trombones = "big parade"
more$ = 1000000
class = "Computer Science 101"
```


      File "<ipython-input-15-d986444d5555>", line 2
        more$ = 1000000
            ^
    SyntaxError: invalid syntax



Mind the difference:

* the cell with `print(Variable` yields a runtime error
* the cell with the illegal variable names gives a `SyntaxError`!

## How to ask for help

We have used the instruction `print("some string")` above, and you have already seen `print` in the "Hello Word" program we ran in clas. The word `print` is a call to a **function**. We examine functions in detail [below](#Functions).


```python
print("hello, world!")
```

    hello, world!


`print` is a [built-in function](https://docs.python.org/3/library/functions.html#print) that (not surprisingly) can be used to print some values to the screen. As many function, it takes argument (in this case, the value to print). As with all the functions, you can inspect what it does and what parameters it takes in a few ways (one is Jupyter only):

* type `help(name-of-function)`
* go online to read the official documentation: https://docs.python.org/3/
* (in Jupyter) type the name of the function and the parentheses: `print()` then, placing the cursor between the parentheses, press Shift+Tab: a contextual menu with help will pop up


```python
help(print)
```

    Help on built-in function print in module builtins:
    
    print(...)
        print(value, ..., sep=' ', end='\n', file=sys.stdout, flush=False)
        
        Prints the values to a stream, or to sys.stdout by default.
        Optional keyword arguments:
        file:  a file-like object (stream); defaults to the current sys.stdout.
        sep:   string inserted between values, default a space.
        end:   string appended after the last value, default a newline.
        flush: whether to forcibly flush the stream.
    


Here is the online documentation (yes, in Jupyter you can display some HTML pages from the web):


```python
from IPython.display import IFrame
IFrame('https://docs.python.org/3/library/functions.html#print', width=800, height=350)
```





        <iframe
            width="800"
            height="350"
            src="https://docs.python.org/3/library/functions.html#print"
            frameborder="0"
            allowfullscreen
        ></iframe>
        



## More on data types

What types of values can we put into a variable? What goes into the shoebox? We can start by the members of this list:

* **Integers** (-1,0,1,2,3,4...)
* **Strings** ("Hello", "s", "Wolfgang Amadeus Mozart", "I am the α and the ω!"...)
* **floats** (3.14159; 2.71828...)
* **Booleans** (True, False)

If you're not sure what type of value you're dealing with, you can use the function `type()`. Yes, it works with variables too...!


```python
st = "I am the α and the ω!"
type(st)
```




    str




```python
type(2.7182818284590452353602874713527)
```




    float




```python
type(True)
```




    bool



## Strings

You declare strings with single ('') or double ("") quote: it's totally indifferent! But now two questions:
1. what happens if you forget the quotes?
2. what happens if you put quotes around a number?


```python
hello = "goodbye"
print(hello)
```

    goodbye



```python
print("hello")
```

    hello



```python
type("2")
```




    str



String, integer, float... Why is that so important? Well, try to sum two strings and see what happens...


```python
"2" + "3"
```




    '23'




```python
#probably you wanted this...
int("2") + int("3")
```




    5



But if we are working with strings, then the "+" sign is used to concatenate the strings:


```python
a = "interesting!"
print("not very " + a)
```

    not very interesting!


And now for something a bit different...


```python
print(2*3)
print("ah" * 3)
```

    6
    ahahah



```python
s = "Catch me if you can! I am not a meme"
s.find("me")
```




    6




```python
s.find("Plato")
```




    -1



## Collections

### Lists

Lists and dictionaries are two very useful types to store whole collections of data


```python
beatles = ["John", "Paul", "George", "Ringo"]
type(beatles)
```




    list



Items in list are accessible using their index. **Do remember** that indexing starts from 0!


```python
print(beatles[0])
```

    John



```python
#indexes can be negative!
beatles[-1]
```




    'Ringo'



![caption](https://i.stack.imgur.com/vIKaD.png)

### Methods to work with lists

There are a bunch of methods that you can apply to list to work with them.

You can append items at the end of a list


```python
beatles.append("Billy Preston")
beatles
```




    ['John', 'Paul', 'George', 'Ringo', 'Billy Preston']



You can learn the index of an item


```python
beatles.index("George")
```




    2



You can insert elements at a predefinite index:


```python
beatles.insert(0, "Pete Best")
print(beatles.index("George"))
beatles
```

    3





    ['Pete Best', 'John', 'Paul', 'George', 'Ringo', 'Billy Preston']



### Slicing a list

But most importantly, you can **slice** lists, producing sub-lists by specifying the range of indexes you want:


```python
beatles[1:5]
```




    ['John', 'Paul', 'George', 'Ringo']



Do you notice something strange? Yes, the limit index is **not** inclusive (i.e. item beatles[5] is not included)


```python
beatles[5]
```




    'Billy Preston'



What happens if you specify an index that is too high?


```python
beatles[7]
```


    ---------------------------------------------------------------------------

    IndexError                                Traceback (most recent call last)

    <ipython-input-20-b9b16afb11e9> in <module>()
    ----> 1 beatles[7]
    

    IndexError: list index out of range


How can you know how long a list is?


```python
len(beatles)
```




    6



Do remember that indexing starts at 0, so don't make the mistake of thinking that `len(yourlist)` will give you the index of the last item of your list!


```python
beatles[len(beatles)]
```


    ---------------------------------------------------------------------------

    IndexError                                Traceback (most recent call last)

    <ipython-input-22-00884e355894> in <module>()
    ----> 1 beatles[len(beatles)]
    

    IndexError: list index out of range


This will work!


```python
beatles[len(beatles) -1]
```




    'Billy Preston'



There is even a third argument that you can put between square brackets: it's the increment, a number that will tell how many steps to move while slicing. The following code, for instance, may be used to yield every element with odd-number indexes up to (but not included) 10


```python
l = [0,1,2,3,4,5,6,7,8,9,10]
l[1:10:2]
```




    [1, 3, 5, 7, 9]



Again, positive index will traverse the list from the lowest to the highest index. Negative indexes go in the opposite direction. So what would be the result of the following code?


```python
l[::-1]
```




    [10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0]



## Dictionaries

Dictionaries are collections of *key : value* pairs. You access the value using the key as index


```python
# dictionaries collections of key : value pairs
beatles_dictionary = { "john" : "John Lennon" ,
                      "paul" : "Paul McCartney",
                      "george" : "George Harrison",
                      "ringo" : "Ringo Starr"}
type(beatles_dictionary)
```




    dict




```python
beatles_dictionary["john"]
```




    'John Lennon'




```python
beatles_dictionary[0]
```


    ---------------------------------------------------------------------------

    KeyError                                  Traceback (most recent call last)

    <ipython-input-28-31e6fcd3d0e7> in <module>()
    ----> 1 beatles_dictionary[0]
    

    KeyError: 0


What can you do with dictionaries (or lists or anything else)? All this built-in objects have predefined methods that help you work with them. We have already seen some built-in methods for lists (`append`). To get a list of attributes and methods of an object in python you can always:

* ask for help in the way we saw above
* use a special function: `dir(name-of-object)`

As argument to `dir` you can use the class: `dir(dict)` in this case, or even the specific object you want to ask about. So for instance you can pass the name of a variable where you saved your dictionary!


```python
#dir(dict)
dir(beatles_dictionary)
```




    ['__class__',
     '__contains__',
     '__delattr__',
     '__delitem__',
     '__dir__',
     '__doc__',
     '__eq__',
     '__format__',
     '__ge__',
     '__getattribute__',
     '__getitem__',
     '__gt__',
     '__hash__',
     '__init__',
     '__iter__',
     '__le__',
     '__len__',
     '__lt__',
     '__ne__',
     '__new__',
     '__reduce__',
     '__reduce_ex__',
     '__repr__',
     '__setattr__',
     '__setitem__',
     '__sizeof__',
     '__str__',
     '__subclasshook__',
     'clear',
     'copy',
     'fromkeys',
     'get',
     'items',
     'keys',
     'pop',
     'popitem',
     'setdefault',
     'update',
     'values']



## Other collections: tuples and sets

Tuples are like lists, but they are immutable. That means that:

* they are accessed like lists, but they have fewer methods: you can't append to or pop from tuple
* they are a bit more performant: they take a little less space when they're initialized
* they may be useful because... they're immutable!



```python
tup = (2,3)
dir(tup)
```




    ['__add__',
     '__class__',
     '__contains__',
     '__delattr__',
     '__dir__',
     '__doc__',
     '__eq__',
     '__format__',
     '__ge__',
     '__getattribute__',
     '__getitem__',
     '__getnewargs__',
     '__gt__',
     '__hash__',
     '__init__',
     '__init_subclass__',
     '__iter__',
     '__le__',
     '__len__',
     '__lt__',
     '__mul__',
     '__ne__',
     '__new__',
     '__reduce__',
     '__reduce_ex__',
     '__repr__',
     '__rmul__',
     '__setattr__',
     '__sizeof__',
     '__str__',
     '__subclasshook__',
     'count',
     'index']



**Sets** are unordered collections with no duplicate elements. Basic uses include membership testing and eliminating duplicate entries. Sets are created with curly braces around a list. Let's see what happens:


```python
aset = {"banana","apple","orange","nuts","apple"}
#how many apples do we have in our set?
aset
```




    {'apple', 'banana', 'nuts', 'orange'}



Sets support all the operations used with [mathematical sets](https://en.wikipedia.org/wiki/Set_(mathematics)). But they are especially useful if, for instance, you have a long list with many duplicates and you only want to work on the unique values. For instance, how many *different* word are there in this sentence?


```python
sent = "Sets are one of the most fundamental concepts in mathematics Developed at the end of the 19th century set theory is now a ubiquitous part of mathematics".split(" ")
print(len(sent))
print(len(set(sent)))
```

    27
    22


# Conditional statements and iterations

## If-statements

Most of the times, what you want to do when you program is to check a value and execute some operation depending on whether the value matches some condition. That's where **if statements** help!

In its easiest form, an If statement is syntactic construction that checks whether a condition is met; if it is some part of code is executed


```python
bassist = "Paul McCartney"

if bassist == "Paul McCartney":
    print("Paul played bass with the Beatles!")
```

    Paul played bass with the Beatles!


Mind the **indentation** very much! This is the essential element in the syntax of the statement


```python
bassist = "Bill Wyman"

if bassist == "Paul McCartney":
    print("I'm part of the if statement...")
    print("Paul played bass in the Beatles!")
```

What happens if the condition is not met? Nothing! The indented code is not executed, because the condition is not met, so lines 4 and 5 are simply skipped.

**But what happens if we de-indent line 5**? Can you guess why this is what happes?

Most of the time, we need to specify what happens if the conditions are not met


```python
bassist = ""

if bassist == "Paul McCartney":
    print("Paul played bass in the Beatles!")
else:
    print("This guy did not play for the Beatles...")
```

    This guy did not play for the Beatles...


This is the flow:
* the condition in line 3 is checked
* is it met?
    * **yes**: then line 4 is executed
    * **no**: then line 6 is executed

Or we can specify many different conditions...


```python
bassist = "Bill"

if bassist == "Paul McCartney":
    print("Paul played bass in the Beatles!")
elif bassist == "Bill Wyman":
    print("Bill Wyman played for the Rolling Stones!")
else:
    print("I don't know what band this guy played for...")
```

    I don't know what band this guy played for...


##  For loops

The greatest thing about lists is that thet are **iterable**, that is you can loop through them. What do we do if we want to apply some line of code to each element in a list? Try with a for loop!

A **for loop** can be paraphrased as: "for each element named x in an iterable (e.g. a list): do some code (e.g. print the value of x)"


```python
for b in beatles:
    print(b + " was one of the Beatles")
```

    Pete Best was one of the Beatles
    John was one of the Beatles
    Paul was one of the Beatles
    George was one of the Beatles
    Ringo was one of the Beatles
    Billy Preston was one of the Beatles


Let's break the code down to its parts:
* **b**: an arbitrary name that we give to the variable holding every value in the loop (it could have been any name; b is just very convenient in this case!)
* **beatles**: the list we're iterating through
* **:** as in the if-statements: don't forget the colon!
* **indent**: also, don't forget to indent this code! it's the only thing that is telling python that line 2 is part of the for loop!
* **line 2**: the function that we want to execute for each item in the iterables

Now, let's join if statements and for loop to do something nice...


```python
beatles = ["John", "Paul", "George", "Ringo"]
for b in beatles:
    if b == "Paul":
        instrument = "bass"
    elif b == "John":
        instrument = "rhythm guitar"
    elif b == "George":
        instrument = "lead guitar"
    elif b == "Ringo":
        instrument = "drum"
    print(b + " played " + instrument + " with the Beatles")
```

    John played rhythm guitar with the Beatles
    Paul played bass with the Beatles
    George played lead guitar with the Beatles
    Ringo played drum with the Beatles


For-loops are an incredibly practical construction to work with any [iterable](https://docs.python.org/3/glossary.html#term-iterable), i.e. any object capable of returning its members one at a time. Strings are iterable too:


```python
for s in "that's fantastic!":
    print(s)
```

    t
    h
    a
    t
    '
    s
     
    f
    a
    n
    t
    a
    s
    t
    i
    c
    !


And if something is not iterable, or the default one is not the iteration you want, you can always use the `split` method of strings!


```python
for value in "lots,of,comma,separated,values".split(","):
    print(value)
```

    lots
    of
    comma
    separated
    values


### Appendix: Unpacking values from a collection

When you have more than 1 value to unpack you can assign it right away to a series of variables. You "unpack" the values e.g. of tuple like this


```python
a,b = ("apple", "orange")
print("a is an " + a + " while b is an " + b)
#actually, there's a more elegant way to insert variables into string! We'll see..
```

    a is an apple while b is an orange


But be careful that you're preparing the right number of variables! What do you think will happen if you run the following?


```python
a,b,c = ("apple", "orange")
```


    ---------------------------------------------------------------------------

    ValueError                                Traceback (most recent call last)

    <ipython-input-39-581e8ee15dd0> in <module>()
    ----> 1 a,b,c = ("apple", "orange")
    

    ValueError: not enough values to unpack (expected 3, got 2)



```python
a,b,c = ("apple", "orange", "banana", "pineapple")
```


    ---------------------------------------------------------------------------

    ValueError                                Traceback (most recent call last)

    <ipython-input-40-ce9b75a0ec8e> in <module>()
    ----> 1 a,b,c = ("apple", "orange", "banana", "pineapple")
    

    ValueError: too many values to unpack (expected 3)


### Appendix: looping through a list getting value an index

Sometimes it's useful to loop through a list getting a value and also the index (i.e. the number of the position) of the element you're evaluating.

You can do it yourself, initializing a variable that works as a counter which starts at 0 and is incremented by 1 at each pass:


```python
i = 0
for fruit in ["apple", "orange", "banana", "pineapple"]:
    print(i, fruit)
    i = i + 1
    #this last line can also be written: i += 1
```

    0 apple
    1 orange
    2 banana
    3 pineapple


But this operation is so common that there is a dedicated function in the Standard Library called `enumerate`. `enumerate` returns an iterable that for every item gives a tuple `(index,value)`. You can unpack it in the way shown [above](#Appendix:-Unpacking-values-from-a-collection):


```python
for i,fruit in enumerate(["apple", "orange", "banana", "pineapple"]):
    print(i,fruit)
```

    0 apple
    1 orange
    2 banana
    3 pineapple


## While loops

While-loops are a more flexible type of loops. Like `for`, `while` allows you to iterate a cycle of instruction, but unlike the for-loop, a while loop does not fix in advance the number of iteration it performs.

Basically, just like the English word "while" suggests, a while loop keeps performing the indented code *while* a specified condition is evaluated as True. As soon as the condition becomes False the loop stops and the rest of the program is executed.

This means that, unlike with for statements that will end as soon as the iterable object is traversed, you as programmer have to state a condition for the loop to continue or stop.

Here is one typical use of the while loop, keep doing something (like adding the consecutive numbers) until a counter variable has reached a certain limit


```python
counter = 5
```


```python
theSum  = 0
aNumber = 1
while aNumber <= counter:
    theSum = theSum + aNumber
    aNumber = aNumber + 1
    #the previous line can also be written as:
    #aNumber += 1
print(theSum)
```

    15


While loops may be a bit more dangerous and difficult to handle. Can you guess why? Why then use them? Well, sometimes they can be very practical and sometimes you just don't know how many iteration you need to perform...

Do you remember the [Notebook](https://github.com/LibraryOfCongress/data-exploration/blob/master/LOC.gov%20JSON%20API.ipynb) from the Library of Congress? There was a cell that shows how to continue requesting the pages with the titles of the Digital Collections as long as there are pages. It made a rather more sophisticated use of the while loop!


```python
import requests
collections_json = requests.get("https://www.loc.gov/collections/?fo=json").json()

while True: #As long as we have a next page, go and fetch it
    for collection in collections_json["results"]: 
        print(collection["title"]) #print out the collection's title
    next_page = collections_json["pagination"]["next"] #get the next page url
    if next_page is not None: #make sure we haven't hit the end of the pages
        collections_json = requests.get(next_page).json()
    else:
        break #we are done and can stop looping
```

    Aaron Copland Collection
    Abdul Hamid II Collection
    Abraham Lincoln Papers at the Library of Congress
    Afghanistan Web Archive
    African American Perspectives: Pamphlets from the Daniel A. P. Murray Collection, 1818-1907
    African American Photographs Assembled for 1900 Paris Exposition
    African-American Band Music & Recordings, 1883-1923
    After the Day of Infamy: "Man-on-the-Street" Interviews Following the Attack on Pearl Harbor
    Alan Lomax Collection
    Alan Lomax Collection of Michigan and Wisconsin Recordings
    Albert Schatz Collection
    Alexander Graham Bell Family Papers at the Library of Congress
    Alexander Hamilton Papers
    Alexander Hamilton Stephens Papers
    The Alfred Whital Stern Collection of Lincolniana
    Amazing Grace
    America at Work, America at Leisure: Motion Pictures from 1894-1915
    America Singing: Nineteenth-Century Song Sheets
    An American Ballroom Companion: Dance Instruction Manuals, ca. 1490-1920
    American Choral Music
    American Colony in Jerusalem, 1870-2006
    American English Dialect Recordings: The Center for Applied Linguistics Collection
    American Leaders Speak: Recordings from World War I
    American Life Histories: Manuscripts from the Federal Writers' Project, 1936-1940
    American Notes: Travels in America, 1750-1920
    American Revolution and Its Era: Maps and Charts of North America and the West Indies, 1750-1789
    American Variety Stage: Vaudeville and Popular Entertainment, 1870-1920
    Andrew Jackson Papers
    Anna Maria Brodeau Thornton Papers
    Ansel Adams's Photographs of Japanese-American Internment at Manzanar
    Architecture, Design & Engineering Drawings
    Archive of Hispanic Literature on Tape
    Archive of Recorded Poetry and Literature
    Bain Collection
    Ballets Russes de Serge Diaghilev
    Band Music from the Civil War Era
    Baseball Cards
    Baseball Sheet Music
    Before and After the Great Earthquake and Fire: Early Films of San Francisco, 1897-1916
    Born in Slavery: Slave Narratives from the Federal Writers' Project, 1936-1938
    Brady-Handy Collection
    Brazil Cordel Literature Web Archive
    Brazilian Presidential Election 2010 Web Archive
    Bronislava Nijinska Collection
    Brumfield Collection
    Buckaroos in Paradise: Ranching Culture in Northern Nevada, 1945-1982
    Burma/Myanmar General Election 2010 Web Archive
    By Popular Demand: Jackie Robinson and Other Baseball Highlights, 1860s-1960s
    Cabinet of American Illustration
    California as I Saw It: First-Person Narratives of California's Early Years, 1849-1900
    California Gold: Northern California Folk Music from the Thirties Collected by Sidney Robertson Cowell
    Cambini Quintets
    Capital and the Bay: Narratives of Washington and the Chesapeake Bay Region, 1600-1925
    Captain Pearl R. Nye: Life on the Ohio and Erie Canal
    The Carnegie Hall Collection of Conversations with Composers
    Carnegie Survey of the Architecture of the South
    Carpenter Collection
    Cartoon Drawings
    Cartoon Drawings: Herblock Collection
    Cartoon Drawings: Swann Collection of Caricature and Cartoon
    Cartoon Prints, American
    Cartoon Prints, British
    Case Books
    A Century of Lawmaking for a New Nation: U.S. Congressional Documents and Debates 1774-1875
    Charles Wellington Reed Papers, 1776 to 1926
    Chicago Ethnic Arts Project Collection
    Chronicling America
    Cities and Towns
    Civil Rights History Project
    Civil War
    Civil War Glass Negatives and Related Prints
    Civil War Maps
    Civil War Sheet Music Collection
    Civil War Soldier in the Wild Cat Regiment : Selections from the Tilton C. Reynolds Papers
    Clara Barton Papers
    College Women's Association of Japan Print Show Collection
    Confederate States of America Records
    Coptic Orthodox Liturgical Chant and Hymnody
    Country Studies
    Crisis in Darfur 2006 Web Archive
    Curtis (Edward S.) Collection
    Daguerreotypes
    The Danny Kaye and Sylvia Fine Collection
    Dayton C. Miller Collection
    Detroit Publishing Company
    Discovery and Exploration
    Documents from the Continental Congress and the Constitutional Convention, 1774-1789
    Dolly Parton and the Roots of Country Music
    Drawings (Documentary)
    Drawings (Master)
    Early American Sheet Music
    Edwin McMasters Stanton Papers, 1818-1921
    Egypt 2008 Web Archive
    Elliott Carter Collection
    Emile Berliner and the Birth of the Recording Industry
    Ernest Bloch Collection
    The Evolution of the Conservation Movement
    Farm Security Administration/Office of War Information Black-and-White Negatives
    Farm Security Administration/Office of War Information Color Photographs
    Federal Courts Web Archive
    Federal Register
    Felix Mendelssohn at the Library of Congress
    Fenton Crimean War Photographs
    Fiddle Tunes of the Old Frontier: The Henry Reed Collection
    Finding Our Place in the Cosmos: From Galileo to Sagan and Beyond
    Fine Prints
    Fine Prints: Japanese, pre-1915
    Florida Folklife from the WPA Collections, 1937-1942
    Franklin Pierce Papers
    Franz Liszt at the Library of Congress
    Frederick Douglass Papers at the Library of Congress
    Freedom's Fortress: The Library of Congress, 1939-1953
    From Slavery to Freedom: The African-American Pamphlet Collection 1822-1909
    Frontline Diplomacy: The Foreign Affairs Oral History Collection of the Association for Diplomatic Studies and Training
    General Maps
    Genthe Collection
    George S. Patton Papers: Diaries, 1910-1945
    George Washington Papers
    The Gerry Mulligan Collection
    Gladstone Collection of African American Photographs
    Goldstein Foundation Collection--Prints and Drawings
    Gottscho-Schleisner Collection
    Grabill Collection
    Great Conversations in Music
    Groups of Images (LOTs)
    Guide Records
    Hannah Arendt Papers
    Harris & Ewing Collection
    Hidden Treasures at the Library of Congress
    Highsmith (Carol M.) Archive
    Hispano Music and Culture of the Northern Rio Grande: The Juan B. Rael Collection
    Historic American Buildings Survey/Historic American Engineering Record/Historic American Landscapes Survey
    Historic Sheet Music Collection, 1800-1922
    Home Sweet Home: Life in Nineteenth-Century Ohio
    Horydczak Collection
    Hotchkiss Map Collection
    Ilka Kolsky Artwork
    Indian General Election 2009 Web Archive
    Indonesian General Election 2009 Web Archive
    Inside an American Factory: Films of the Westinghouse Works, 1904
    International Tribunals Web Archive
    Inventing Entertainment: The Early Motion Pictures and Sound Recordings of the Edison Companies
    Iraq War 2003 Web Archive
    Irving Fine Collection, ca. 1914-1962
    It's Showtime! Sheet Music from Stage and Screen
    James K. Polk Papers
    James Madison Papers, 1723-1859
    James Monroe Papers
    Japanese-American Internment Camp Newspapers, 1942-1946
    Jazz on the Screen Filmography
    Jedediah Hotchkiss Papers
    Joe Smith
    John J. Pershing Papers
    John Tyler Papers
    Johnston (Frances Benjamin) Collection
    Korab Collection
    Lamb Studios Archive
    Laotian General Election 2011 Web Archive
    Last Days of a President: Films of McKinley and the Pan-American Exposition, 1901
    Lawrence & Houseworth Collection
    Legal Blawgs Web Archive
    Legislative Branch Web Archive
    Leonard Bernstein
    Lewis Carroll Scrapbooks
    Lewis H. Machen Family Papers
    The Library of Congress Celebrates the Songs of America
    Library of Congress Concerts
    The Life of a City: Early Films of New York, 1898-1906
    Liljenquist Family Collection of Civil War Photographs
    Lomax Collection
    Look Collection
    Louisiana: European Explorations and the Louisiana Purchase
    Manuscript Division Web Archive
    Manuscripts in St. Catherine's Monastery, Mount Sinai
    Manuscripts in the Libraries of the Greek and Armenian Patriarchates in Jerusalem
    Mapping the National Parks
    Maps of Liberia, 1830-1870
    The March King: John Philip Sousa
    Margaret Bayard Smith Papers
    Martha Graham at the Library of Congress
    Martin Van Buren Papers, 1787 to 1910
    Mary Ann Bickerdyke Papers
    Matson (G. Eric and Edith) Photograph Collection
    Military Battles and Campaigns
    Millard Fillmore Papers
    Miller NAWSA Suffrage Scrapbooks, 1897-1911
    The Moldenhauer Archives - The Rosaleen Moldenhauer Memorial
    Montana Folklife Survey Collection
    Music for the Nation: American Sheet Music, ca. 1820-1860
    Music for the Nation: American Sheet Music, ca. 1870-1885
    Music Treasures Consortium
    Musical Instruments at the Library of Congress
    Nathan W. Daniels Diary and Scrapbook
    National American Woman Suffrage Association Collection
    National Child Labor Committee Collection
    National Jukebox
    National Photo Company Collection
    New York Journal and Related Titles, 1896-1899
    Newspaper Pictorials: World War I Rotogravures, 1914-1919
    Now What a Time: Blues, Gospel, and the Fort Valley Music Festivals, 1938-1943
    Occupational Folklife Project
    Omaha Indian Music
    Origins of American Animation
    Panoramic Maps
    Panoramic Photographs
    Papal Transition 2005 Web Archive
    Patriotic Melodies
    PH Filing Series Photographs
    Philip Henry Sheridan Papers
    Philippine General Election 2010 Web Archive
    Phillips/Mathée Collection
    Photochrom Prints
    Pioneering the Upper Midwest: Books from Michigan, Minnesota, and Wisconsin, ca. 1820-1910
    Polish Declarations of Admiration and Friendship for the United States, 1926
    Popular Graphic Arts
    Posters: Artist Posters
    Posters: Performing Arts Posters
    Posters: Spanish Civil War Posters
    Posters: World War I Posters
    Posters: WPA Posters
    Posters: Yanker Poster Collection
    Prairie Settlement: Nebraska Photographs and Family Letters, 1862-1912
    Pre-1700 Musical Treasures: Manuscript and Print Collection
    Printed Ephemera: Three Centuries of Broadsides and Other Printed Ephemera
    Prokudin-Gorskii Collection
    Prosperity and Thrift: The Coolidge Era and the Consumer Economy, 1921-1929
    Public Policy Topics Web Archive
    Puerto Rico at the Dawn of the Modern Age: Nineteenth- and Early-Twentieth-Century Perspectives
    Quilts and Quiltmaking in America, 1978-1996
    Ragtime
    Railroad Maps, 1828-1900
    Rare Book Selections
    Rhode Island Folklife Project Collection
    Rochambeau Map Collection
    The Roger Reynolds Collection
    Roman Totenberg Papers
    Roman Totenberg Papers: Totenberg-Wilk Holocaust Material
    Rosa Parks Papers
    Salmon P. Chase Papers
    Samuel Barber at the Library of Congress
    Samuel F. B. Morse Papers at the Library of Congress, 1793-1919
    Samuel J. Gibson Diary and Correspondence
    Sanborn Maps
    Selections from the Katherine Dunham Collection
    September 11, 2001 Web Archive
    September 11, 2001, Documentary Project
    Show Music on Record
    Sigmund Freud Papers
    Sikkim Photos (Kandell Collection)
    Sir Francis Drake (Kraus Collection)
    Slaves and the Courts, 1740-1860
    Small Press Expo Comic and Comic Art Web Archive
    South Central Georgia Folklife Project Collection
    Southern Mosaic: The John and Ruby Lomax 1939 Southern States Recording Trip
    The Spalding Base Ball Guides, 1889-1939
    The Spanish-American War in Motion Pictures
    Sports Byline
    Sri Lankan Presidential and General Elections 2010 Web Archive
    Stars and Stripes: The American Soldiers' Newspaper of World War I, 1918-1919
    Stereograph Cards
    Tending the Commons: Folklife and Landscape in Southern West Virginia
    Thai General Election 2011 Web Archive
    Theodore Roosevelt: His Life and Times on Film
    Thomas Biggs Harned Collection of Walt Whitman Papers
    Thomas Jefferson Papers, 1606-1827
    Tibetan Oral History Archive Project
    Timor Leste 2010-2011 Web Archive
    Tissandier Collection
    Today in History
    Traditional Music and Spoken Word
    Transit of Venus March
    Transportation and Communication
    Ulysses S. Grant Papers
    United States Congressional Web Archive
    United States Elections Web Archive
    Van Vechten Collection
    Veterans History Project
    Vietnam-Era Prisoner-of-War/Missing-in-Action Database
    Vietnamese General Election 2011 Web Archive
    Visual Image Web Archive
    Voices from the Days of Slavery: Former Slaves Tell Their Stories
    Voices from the Dust Bowl: the Charles L. Todd and Robert Sonkin Migrant Worker Collection, 1940-1941
    Walt Whitman Papers (Miscellaneous Manuscript Collection)
    Walt Whitman Papers in the Charles E. Feinberg Collection
    Warren G. Harding-Carrie Fulton Phillips Correspondence
    Washington During the Civil War: The Diary of Horatio Nelson Taft, 1861-1865
    Web Cultures Web Archive
    Webcomics Web Archive
    Wilbur and Orville Wright Papers at the Library of Congress
    William Henry Harrison Papers
    William P. Gottlieb Collection
    William Speiden Journals
    William T. Sherman Papers
    Winter Olympic Games 2002 Web Archive
    Wm. Oland Bourne Papers
    Women of Protest: Photographs from the Records of the National Woman's Party
    Woody Guthrie and the Archive of American Folk Song: Correspondence, 1940-1950
    Working in Paterson: Occupational Heritage in an Urban Setting
    World War I Sheet Music
    World War II Military Situation Maps
    World's Transportation Commission
    Wright Brothers Negatives
    Yiddish American Popular Sheet Music
    Zachary Taylor Papers
    Zora Neale Hurston Plays at the Library of Congress


# Input and Output

One of the most frequent tasks that programmers do is reading data from files, and write some of the output of the programs to a file. 

In Python (as in many language), we need first to open a file-handler with the appropriate mode in order to process it. Files can be opened in:
* read mode ("r")
* write mode ("w")
* append mode

Let's try to read the content of one of the txt files of our Sunoikisis directory

First, we open the file handler in **read mode**:


```python
#see? we assign the file-handler to a variable, or we wouldn't be able
#to do anything with that!
f =  open("../DATA/Session3/pride.txt", "r")
```

note that **"r" is optional**: read is the default mode!

Now there are a bunch of things we can do:
* read the full content in one variable with this code:

```Python
content = f.read()
```

* read the lines in a list of lines:

```Python
lines = f.readlines()
```

* or, which is the easiest, simply read the content one line at the time with a for loop; the **f object is iterable**, so this is as easy as:

```Python
for l in f:
    doStuffWith(l) #do whatever you want with the line
```

Once you're done, don't forget to **close the handle**:


```python
f.close()
```


```python
#all together
f = open("../DATA/Session3/NOTES.md")
for l in f:
    print(l)
f.close()
```

    # NOTES
    
    This is a sample file that we want to have fun with while learning some Python.
    
    
    
    Python is a lot of fun because:
    
    
    
    * I was invented by a Dutch guy, [Guido van Rossum](https://en.wikipedia.org/wiki/Guido_van_Rossum)
    
    * It has a whole philosophy behing
    
    * it's awasome!
    


Now, there's a shortcut statement, which you'll often see and is very convenient, because it takes care of opening, closing and cleaning up the mess, in case there's some error:


```python
with open("../DATA/Session3/NOTES.md") as f:
    #mind the indent!
    for l in f:
        #double indent, of course!
        print(l)
```

    # NOTES
    
    This is a sample file that we want to have fun with while learning some Python.
    
    
    
    Python is a lot of fun because:
    
    
    
    * I was invented by a Dutch guy, [Guido van Rossum](https://en.wikipedia.org/wiki/Guido_van_Rossum)
    
    * It has a whole philosophy behing
    
    * it's awasome!
    


Now, how about **writing** to a file? Let's try to write a simple message on a file; first, we open the handler in write mode


```python
out = open("test.txt", "w")
```


```python
#the file is now open; let's write something in it
out.write("This is a test!\nThis is a second line (separated with a new-line feed)")
```




    70



The file has been created! Let's check this out


```python
#don't worry if you don't understand this code!
#We're simply listing the content of the current directory...
import os
os.listdir()
```




    ['Sunoikisis - Named Entity Extraction 1a-VV.ipynb',
     'requirements.txt',
     'Sunoikisis - Named Entity Extraction 1a.ipynb',
     'outline.md',
     'Sunoikisis - Named Entity Extraction 1a_MR.ipynb',
     'Sunoikisis - Named Entity Extraction 1b.ipynb',
     '.git',
     'Sunoikisis - Named Entity Extraction 1b_MR.ipynb',
     '.ipynb_checkpoints',
     'LICENSE',
     'test.txt',
     'Sunoikisis - Named Entity Extraction 1b-VV.ipynb',
     '.gitignore',
     'imgs',
     'README.md',
     'NOTES.md']



But before we can do anything (e.g. open it with your favorite text editor) you have to **close the file-handler**!


```python
out.close()
```

Let's look at its content


```python
with open("test.txt") as f:
    print(f.read())
```

    This is a test!
    This is a second line (separated with a new-line feed)


Again, also for writing we can use a **with statement**, which is very handy.

But let's have a look at what happens here, so we understand a bit better why **"write mode" must be used carefully!**


```python
with open("test.txt", "w") as out:
    out.write("Oooops! new content")
```

Let's have a look at the content of "test.txt" now


```python
with open("test.txt") as f:
    print(f.read())
```

    Oooops! new content


See? After we opened the file in "write mode" for the second time, **all content of the file was erased** and replaced with the new content that we wrote!!!

So keep in mind: when you open a file in "w" mode:

* if it doesn't exist, a new file with that name is created
* if it does exist, it is completely overwritten and all previous content is lost

If you want to write content to an existing file without losing its pervious content, you have to open the file with the "a" mode:


```python
with open("test.txt", "a") as out:
    out.write('''\nAnd this is some additional content.
The new content is appended at the bottom of the existing file''')
```


```python
with open("test.txt") as f:
    print(f.read())
```

    Oooops! new content
    And this is some additional content.
    The new content is appended at the bottom of the existing file


# Functions

Above, we have opened a file several times to inspect its content. Each time, we had to type the same code over and over. This is the typical case where you would like to save some typing (and write code that is much easier to maintain!) by defining a **function**

A function is a block of reusable code that can be invoked to perform a definite task. Most often (but not necessarily), it accepts one or more arguments and return a certain value.

We have already seen one of the built-in functions of Python: `print("some str")`

But it's actually very easy to define your own. Let's define the function to print out the file content, as we said before. Note that this function takes one argument (the file name) and prints out some text, but doesn't return back any value.


```python
def printFileContent(file_name):
    #the function takes one argument: file_name
    with open(file_name) as f:
        print(f.read())
```

As usual, mind the indent!

`file_name` (line 1) is the placeholder that we use in the function for any argument that we want to pass to the function in our real-life reuse of the code.

Now, if we want to use our function we simply call it with the file name that we want to print out


```python
printFileContent("README.md")
```

    # SunoikisisDC_NER
    The materials for the SunoikisisDC sessions on Named Entity Extraction (2016/2017) 
    


Now, let's see an example of a function that returns some value to the users. Those functions typically take some argument, process them and yield back the result of this processing.

Here's the easiest example possible: a function that takes two numbers as arguments, sum them and returns the result.


```python
def sumTwoNumbers(first_int, second_int):
    s = first_int + second_int
    return s
```


```python
#could be even shorter:
def sumTwoNumbers(first_int, second_int):
    return first_int + second_int
```


```python
sumTwoNumbers(5, 6)
```




    11



Most often, you want to assign the result returned to a variable, so that you can go on working with the results...


```python
s = sumTwoNumbers(5,6)
s * 2
```




    22



# Error and exceptions

Things can go wrong, especially when you're a beginner. But no panic! Errors and exceptions are actually a good thing! Python gives you detailed reports about what is wrong, so read them carefully and try to figure out what is not right.

Once you're getting better, you'll actually learn that you can do something good with the exceptions: you'll learn how to handle them, and to anticipate some of the most common problems that dirty data can face you with...

Now, what happens if you forget the **all-important syntactic constraint of the code indent**?


```python
if 1 > 0:
    print("Well, we know that 1 is bigger than 0!")
```

    Well, we know that 1 is bigger than 0!


Pretty clear, isn't it? What you get is an **syntax error** a construct that is not grammatical in Python's syntax. Note that you're also told where (at what line, and at what point of the code) your error is occurring. That is not always perfect (there are cases where the problem is actually occuring before what Python thinks), but in this case it's pretty OK.

What if you forget to define a variable (or you misspell the name of a variable)?


```python
var = "bla bla"
if var1:
    print("If you see me, then I was defined...")
```


    ---------------------------------------------------------------------------

    NameError                                 Traceback (most recent call last)

    <ipython-input-84-f4ea1c51d2f6> in <module>()
          1 var = "bla bla"
    ----> 2 if var1:
          3     print("If you see me, then I was defined...")


    NameError: name 'var1' is not defined


You get an **exception**! The syntax of your code is right, but the execution met with a problem that caused the program to stop.

Now, in your program, you can handle selected exception: this means that you can write your code in a way that the program would still be executed even if a certain exception is raised.

Let's see what happens if we use our function to try to print the content of a file that doesn't exist:


```python
printFileContent("file_that_is_not_there.txt")
```


    ---------------------------------------------------------------------------

    FileNotFoundError                         Traceback (most recent call last)

    <ipython-input-85-501b445822cd> in <module>()
    ----> 1 printFileContent("file_that_is_not_there.txt")
    

    <ipython-input-71-09207a2f323d> in printFileContent(file_name)
          1 def printFileContent(file_name):
          2     #the function takes one argument: file_name
    ----> 3     with open(file_name) as f:
          4         print(f.read())


    FileNotFoundError: [Errno 2] No such file or directory: 'file_that_is_not_there.txt'


We get a `FileNotFoundError`! Now, let's re-write the function so that this event (somebody uses the function with a wrong file name) is taken care of...


```python
def printFileContent(file_name):
    #the function takes one argument: file_name
    try:
        with open(file_name) as f:
            print(f.read())
    except FileNotFoundError:
        print("The file does not exist.\nNevertheless, I do like you, and I will print something to you anyway...")
```


```python
printFileContent("file_that_doesnt_exist.txt")
```

    The file does not exist.
    Nevertheless, I do like you, and I will print something to you anyway...


# Libraries and modules

A module is a file containing Python definitions and statements intended for use in other Python programs. There are many Python modules that come with Python as part of the standard library. As we already did with the (built-in) function `print` you can check the list and the documentation about the modules in the Python Documentation. Here you'll find the list of the modules installed on the Standard Library (i.e. in the collection of modules that are installed automatically when you install Python):

https://docs.python.org/3/py-modindex.html

The first thing you do when you want to use code and functions defined in a module is to `import` the module.

For example, what if we want to generate a random number comprised between a starting and an end point (let's say between 140 and 155)?

We can use the `randint` function in the `random` module


```python
import random
```


```python
random.randint(101, 155)
```




    106



Note that the function is called by using the pattern:

```
module.function()

```

That is because we imported the module with the simple statement:


```Python
import module
```


An alternative would be to use the following syntax

```Python
from module import function
```

Here for instance we import and use the function `sqrt` from the `math` module of the Standard Library, a function that allows you to get the square root of a number


```python
from math import sqrt
sqrt(25)
```




    5.0



But now we have imported, and therefore we can use, **only** this function from the math module. See what happens if we try to use another function from the same module


```python
#wont' work!
log(25,5)
```


```python
#now it will work!
from math import log
log(25, 5)
```




    2.0



You can even combine import statements from the same module in this way:


```python
from math import sqrt, log #and so on with whatever you want to import
```

You can also (and in fact it is very common) select aliases for your modules. For instances:


```python
import math as i_hate_math
```


```python
i_hate_math.sqrt(25)
```




    5.0



Obviously, you're not doing this to be silly (as in the previous example!), but to be more concise. For instance, it is conventional (and you'll find this sytstematically in every tutorial or programs that you find online) to import the most important library for data analysis in Python (called `pandas`) in this way:


```python
import pandas as pd
```

This statement to import *everything* from a module is also legal, but this is **NOT A GOOD WAY** to do it. **Don't use it**:

```Python
from math import *
 ```

# Homeworks!

You'll find a nice exercise in this repository:

https://git.informatik.uni-leipzig.de/introdh17-18/Materials-Exercises/tree/master/Session3

This time you will be asked to design, write and run a short script that performs the following steps:

* Read an input file in the Materials-Exercises repo (`DATA/Seminar3/pride.txt`)
* Scan each line
* Write on an output file **only** the lines where the string or substring "Bennet" ("Bennet's" and "Bennets" included) is found with the following format:

```
line nr. {tab} line
```


Upload your results in the usual way using GitLab. Have fun!

# Appendix: useful links

## Python: how to install

If you're using Mac OSX or Linux, you already have (at least one version of) Python installed. Anyway, it's very easy to install Python or upgrade your version. See:

https://wiki.python.org/moin/BeginnersGuide/Download

## Jupyter: how to install

http://jupyter.org/install.html

Python and Jupyter come also in a pre-packaged environment (which is designed especially for data science) called [Anaconda](https://www.continuum.io/downloads). You might be interested to look at that.

## Python 2 or Python 3? 

Python 3 is the latest version of Python (currently, 3.6.1). It's a major upgrade from Python 2, but the code has been somewhat dramatically changed in the passage from 2 to 3 and there is some problem of backward compatibility. Some version of Linux or Mac OSX still come with Python 2.7 (the final version of Python 2).

Anyway, Python 3 is currently in active development: it's where the cutting-edge improvements and new stuff are being developed (especially for NLP and the NLTK library). In this code, we assume Python 3!

https://wiki.python.org/moin/Python2orPython3

## NLTK: Book

Would you like a book that is a great introduction to Python for absolute beginners, is a wonderfull resource to learn the basics of Natural Language processing and gives you a thorough introduction to the NLTK library to do NLP in Python? Oh, yeah, I was forgetting: that can be read for free on the internet? Yes, it's Christmass time!

http://www.nltk.org/book/
